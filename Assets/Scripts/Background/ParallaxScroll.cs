// Copyright (C) 2023 - Retrocoder Games <j.karlsson@retrocoder.se>

using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class ParallaxScroll : MonoBehaviour {

  private Camera mainCamera;

  private float[] startPosX;
  private float[] spriteWidth;

  // Start is called before the first frame update
  void Start() {
    mainCamera = Camera.main;

    startPosX = new float[transform.childCount];
    spriteWidth = new float[transform.childCount];

    for (int i = 0; i < transform.childCount; i++) {
      Transform bgTransform = transform.GetChild(i);
      startPosX[i] = bgTransform.position.x;
      spriteWidth[i] = bgTransform.GetComponent<SpriteRenderer>().bounds.size.x;
    }
  }

  // Update is called once per frame
  void FixedUpdate() {
    for (int i = 0; i < transform.childCount; i++) {
      Transform bgTransform = transform.GetChild(i);
      float backgroundX = startPosX[i] + mainCamera.transform.position.x * bgTransform.position.z;
      bgTransform.position = new Vector3(backgroundX, bgTransform.position.y, bgTransform.position.z);

      float distanceToCamera = Mathf.Abs(mainCamera.transform.position.x - bgTransform.position.x);
      if (distanceToCamera > 1.5*spriteWidth[i]) {
        if (mainCamera.transform.position.x > bgTransform.position.x) {
          startPosX[i] += 3*spriteWidth[i];
        } else {
          startPosX[i] -= 3*spriteWidth[i];
        }
      }
    }
  }
}

// --- License ---
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>
// or write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

