// Copyright (C) 2023 - Retrocoder Games <j.karlsson@retrocoder.se>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerActions : MonoBehaviour
{
  public InputActionAsset actions;

  Rigidbody2D rb;
  Vector2 moveAmount;

  private void Awake()
  {
    actions.FindActionMap("Player").FindAction("Move").performed += OnMove;
    actions.FindActionMap("Player").FindAction("Move").canceled += OnMove;
  }

  // Start is called before the first frame update
  void Start() {
    rb = GetComponent<Rigidbody2D>();
  }

  // Update is called once per frame
  void Update() {
    if (moveAmount.x != 0)
    {
      rb.velocity = moveAmount*5;
    } else {
      rb.velocity = new Vector2(0,0);
    }
  }

  void OnMove(InputAction.CallbackContext ctx)
  {
    moveAmount = ctx.ReadValue<Vector2>();
  }

  void OnEnable()
  {
    actions.FindActionMap("Player").Enable();
  }
  void OnDisable()
  {
    actions.FindActionMap("Player").Disable();
  }

}

// --- License ---
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>
// or write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

