# Parallax Scroll in Unity
A example project in Unity showing how to create a parallax scroll effect in a 2D game.

# Youtube channel
This is the example code from my youtube video creating the parallax scroller:
Watch it here:
Part 1 - https://youtu.be/X10Imt9TIPw
Part 2 - https://youtu.be/X579_h55PUg

See also my youtube channel
https://www.youtube.com/@retrocoder_games

# Licence
All code in this project is licenced under GPLv2.
See https://www.gnu.org/licenses/

All art and graphics, as well as music and sounds is licenced under Creative Common Licence CC-BY-SA 4.0
See https://creativecommons.org/licenses/by-sa/4.0/

